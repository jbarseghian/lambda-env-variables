package main

import (
	"context"
	"fmt"
	"github.com/aws/aws-lambda-go/lambda"
	"os"
)

func HandleRequest(ctx context.Context) (*string, error) {
	message := fmt.Sprintf("%s is %s. years old\n", os.Getenv("NAME"), os.Getenv("AGE"))
	return &message, nil
}

func main() {
	lambda.Start(HandleRequest)
}
